Research Question Assignment
Dr. John Noll
Monday, 8 Nov, 2021

Overview

This deliverable is the Research Question component of the coursework.
You will submit your research question using Git, so it is important
that you understand how to use Git to commit and upload files to
BitBucket from the command line. Using the BitBucket web interface is
not acceptable and will result in reduced marks for the “Research
Infrastructure” component of the coursework.

To submit the Research Question deliverable successfully, you must
complete three steps:

1.  Identify a dataset.
2.  Formulate a research question, in one of the three forms specified
    in the Research Questions lecture.
3.  Document and submit your research question, using Git.

Each of these steps is described in detail below.

Instructions

Identify a dataset.

1.  Read the research question section of the coursework specification,
    to be sure you understand the requirements for this part of the
    coursework.

2.  Discuss potential research topics among yourselves, and decide on
    some general topics of interest to the group, such as “cricket”,
    “COVID-19”, “happiness”, “crime”, “employment”, etc.

3.  Visit data.world and register for a free (community) account.

4.  Login to data.world.

5.  Search for datasets relevant to your topic.

6.  Choose one to three candidate datasets.

Formulate a Research Question

1.  Look at the columns in your dataset. These are the variables you
    will use when formulating your research question.

2.  Identify the meaning of the variable names. This will help you
    formulate a research question.

3.  Determine which variable will be your independent variable (the one
    that determines the outcome), and which variable will be your
    dependent variable (the one that represents the value of the
    outcome).

4.  Identify the kind of data represented by the independent and
    dependent variables: interval, ordinal, or nominal. This will
    determine what kind of analysis you can do:

    -   If both are interval or ordinal, you may use correlation (but
        only if the independent variable has at least eight distinct
        values).

    -   If the independent variable is nominal, or is ordinal with less
        than eight (8) values, but the dependent variable is interval,
        you may use comparison of means (or medians).

    -   If the independent variable is nominal, or is ordinal with less
        than eight (8) values, and the dependent variable is an interval
        value representing a proportion (fraction or percentage), you
        should use comparison of proportions.

    The Research Questions lecture recordings and notes discuss these
    differences in some detail. Also, you might want to read the marking
    rubric (rubric.xlsx) to see how we will mark your research question
    deliverable.

Document and Submit your Research Question

1.  If you haven’t already done so, clone your group’s BitBucket
    repository into a local workspace.

2.  Change directories to the directory of your local workspace.

3.  Configure your email address. From the command line, type

         git config user.email abc21def@herts.ac.uk

    Be sure to user your Herts email address, so your contributions to
    your project are recorded.

Edit research_question.yml

CAUTION research_question.yml is a “semi-structured” file in YAML
format. YAML is a data serialization language that uses key-value pairs,
similar to those in Windows “INI” files.

Pay careful attention to whitespace, spelling, capitalization, and
structure in the instructions below.

1.  Copy research_question.yml from this instructions workspace to your
    group workspace.

2.  Change to your group workspace, and open research_question.yml.

3.  Write your group name after the group: key.

4.  Write the names (but NOT student IDs) of each group member,
    separated by a comma (‘,’), within the square brackets after the
    members: key.

5.  Write the general topic area from which you derived your research
    question after the topic: key.

6.  Write your research question after the RQ: key. Be sure it is an
    actual question, written following one of the three question
    templates specified in the Research Question lecture recording and
    notes.

    Note: groups who do not express their research question using one of
    the three question templates will fail this assignment.

7.  Write the null hypothesis for your research question after the
    null-hypothesis: key.

8.  Write the alternative hypothesis for your research question after
    the alt-hypothesis: key.

9.  Write the URL of your dataset after the dataset-url: key. This must
    be a URL from https://data.world/community/open-community/, and have
    data.world in the hostname portion of the URL.

10. Download your dataset into your Git workspace, and add it to the
    staged commit using git add DATASET.csv.

11. Load your dataset into R using the read.csv() function:

         > d <- read.csv("DATASET.csv")

    (Replace “DATASET” with the actual name of your dataset file).

12. Print the column names of your dataset using the colnames function:

         > colnames(d)

13. Copy the command invocation and its output, and paste it into the
    space under the columns: | key.

14. Insert two spaces before every line in the output you pasted into
    the space under the columns: | key.

15. Proofread your file to be sure you have spelled everything
    correctly, that you have expressed your research question as a
    question, and that you have followed YAML syntax (Wikipedia has a
    nice summary of YAML syntax if you are curious).

16. Commit your changes using git commit -m "COMMIT LOG MESSAGE".

17. Push your workspace to BitBucket by the deadline (23:59 2021-11-12).

Notes

1.  YAML is a data serialization language. As such, the syntax of the
    research_question.yml file is important:

    -   Keys must be spelled exactly as shown, with exactly the same
        capitalization and spelling.
    -   All keys must be followed by a colon (‘:’) then a space.
    -   The vertical bar after the columns key is meaningful: it means
        the indented text that follows should be formatted exactly as
        written.
    -   You must indent the text after the columns key by two spaces.

2.  Groups who push their research_question.yml file the day before the
    deadline will get feedback about whether their file follows correct
    YAML syntax.

3.  There are only three acceptable formats for research questions in
    this module (7COM1085–Team Research and Development Project). These
    are described in detail in the Research Questions lecture recording
    and accompanying notes.

    Be sure you understand the three formats before you write your
    research question.

4.  The marking rubric (rubric.xlsx) shows how we will mark your
    research_question.yml; test yourself before submitting.

Example

The following example asks about the relationship between happiness and
generosity (do NOT use the same question for your deliverable!).

    # This file is in YAML format.  You must ensure that the punctuation
    # and whitespace is preserved.  Do NOT use Windows Notepad to edit this
    # file; use a real programming editor like Notepad++ instead.

    # Write your group name exactly as it appears on Canvas.
    group: A_group 999

    # Write the names of your group members as a comma-separated list
    # within the square brackets.
    members: [Luke Bisee, Harly Werkin, Notta Klu, Ben Gonna, Abbey Sent]

    # Write your general topic.  
    topic: Happiness 

    # Write your research question, following one of the three allowed templates.
    # Be sure your question is all on one line.
    RQ: Is there a correlation between happiness and generosity?

    # Write the null hypothesis for your research question, again ensuring it is all on one line.
    null-hypothesis: There is no correlation between happiness and generosity.

    # Write the alternative hypothesis for your research question, again ensuring it is all on one line.
    alt-hypothesis: There is a correlation between happiness and generosity.

    # Write the dataset URL.  Be sure it has the allowed hostname.
    dataset-url: https://data.world/juiche/happiness

    # Paste the output of loading your dataset into R, and executing the
    # colnames() function, into the space below the `columns: |` line.
    # Put two spaces *before* every line you pasted.
    columns: |
      > d<-read.csv("2015.csv")
      > colnames(d)
       [1] "Country"                       "Region"                       
       [3] "Happiness.Rank"                "Happiness.Score"              
       [5] "Standard.Error"                "Economy..GDP.per.Capita."     
       [7] "Family"                        "Health..Life.Expectancy."     
       [9] "Freedom"                       "Trust..Government.Corruption."
      [11] "Generosity"                    "Dystopia.Residual"            
